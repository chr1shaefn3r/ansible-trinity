# ansible-trinity

Overview of the existing and enabled roles:

 * autoupgrade
 * packages
 * rsnapshot (half-automated)

## autoupgrade

Use cron-apt to automatically install all package updates

## packages

Install needed packages

## rsnapshot

installs rsnapshot and config, but user has to manually activate it (for now)

## HDD-Configuration

CN0/0: Z300LP2S
CN1/1: Z300NFXH
CN0/1: Z300LMWN
CN1/0: Z304CY6E
CN0/2: Z304BZ80
CN0/3: Z304CZST
CN1/3: Z300NEYX
CN1/2: Z300V0VE
